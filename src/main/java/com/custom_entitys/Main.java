package com.custom_entitys;

import com.custom_entitys.commands.CommandCreate;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

    public static Main instance;
    public static Plugin plugin;

    public static Main getInstance() {
        return Main.instance;
    }

    @Override
    public void onEnable() {
        Main.instance = this;
        Main.plugin = this;
        Main.instance.getCommand("create").setExecutor(new CommandCreate());
    }

    @Override
    public void onDisable() {
        //
    }
}
