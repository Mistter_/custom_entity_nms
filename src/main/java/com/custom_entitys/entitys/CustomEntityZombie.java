package com.custom_entitys.entitys;

import com.custom_entitys.pathfienders.PathfinderGoalZombie;
import net.minecraft.server.v1_16_R2.*;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_16_R2.CraftWorld;
import org.bukkit.craftbukkit.v1_16_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.inventory.ItemStack;

public class CustomEntityZombie extends EntityZombie {

    public CustomEntityZombie(Location location, Player player) {
        super(EntityTypes.ZOMBIE, ((CraftWorld) location.getWorld()).getHandle());
        this.setPosition(location.getX(), location.getY(), location.getZ());
        this.setInvulnerable(true);
        this.setInvisible(true);
        this.setSilent(true);
        this.getBukkitCreature().setInvisible(true);
        this.getBukkitCreature().getEquipment().setHelmet(new ItemStack(Material.GLASS));
        ((CraftPlayer) player).getHandle().startRiding(this);
        this.setGoalTarget((EntityLiving)((CraftPlayer) player).getHandle(), EntityTargetEvent.TargetReason.CUSTOM, true);
    }

    @Override
    public void initPathfinder() {
        this.goalSelector.a(0, new PathfinderGoalFloat(this));
        this.goalSelector.a(1, new PathfinderGoalZombie(this, 1, 16));
        //this.goalSelector.a(2, new PathfinderGoalFloat(this));
    }
}
