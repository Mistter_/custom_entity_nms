package com.custom_entitys.commands;

import com.custom_entitys.entitys.CustomEntityZombie;
import net.minecraft.server.v1_16_R2.ChatComponentText;
import net.minecraft.server.v1_16_R2.WorldServer;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_16_R2.CraftWorld;
import org.bukkit.entity.Player;

public class CommandCreate implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;

        if (command.getName().equalsIgnoreCase("create")) {
            CustomEntityZombie zombie = new CustomEntityZombie(player.getLocation(), player);
            zombie.setCustomName(new ChatComponentText(player.getName()));
            WorldServer world = ((CraftWorld) player.getWorld()).getHandle();
            world.addEntity(zombie);
            player.sendMessage(ChatColor.GREEN + " * Zumbi sumonado!");
        }
        return false;
    }
}
