package com.custom_entitys.pathfienders;

import net.minecraft.server.v1_16_R2.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.util.Vector;

import java.util.EnumSet;

public class PathfinderGoalZombie extends PathfinderGoal {

    private final EntityInsentient a; //zombie
    private EntityLiving b; //player

    private final double f; //entity's speed
    private final float g; //distance between owner and entity

    private double c; //x
    private double d; //y
    private double e; //z

    public PathfinderGoalZombie(EntityInsentient a, double speed, float distance) {
        this.a = a;
        this.f = speed;
        this.g = distance;
        this.a(EnumSet.of(Type.MOVE));
    }

    @Override
    public boolean a() {
        // this will start the pathfindergoal if it's true and will runs every tick
        this.b = this.a.getGoalTarget();

        if (this.b == null) return false;
        else if (this.a.getDisplayName() == null) return false;
        else if (!(this.a.getDisplayName().toString().contains(this.b.getName()))) return false;
        else {
            EntityHuman human = (EntityHuman) a.passengers.get(0);

            float aT = human.aT;
            float aR = human.aR;

            if (aT != 0 || aR != 0) {
                float yaw = human.getBukkitEntity().getLocation().getYaw();

                double x = human.locX();
                double z = human.locZ();

                if(aT < 0) {
                    z -= 2f * Math.cos(Math.toRadians(yaw));
                    x += 2f * Math.sin(Math.toRadians(yaw));
                }
                if(aT > 0) {
                    z += 2f * Math.cos(Math.toRadians(yaw));
                    x -= 2f * Math.sin(Math.toRadians(yaw));
                }
                if(aR < 0) {
                    x -= 2f * Math.cos(Math.toRadians(yaw));
                    z -= 2f * Math.sin(Math.toRadians(yaw));
                }
                if(aR > 0) {
                    x += 2f * Math.cos(Math.toRadians(yaw));
                    z += 2f * Math.sin(Math.toRadians(yaw));
                }

                Location to = new Location(human.getWorld().getWorld(), x, human.getWorld().getWorld().getHighestBlockYAt((int) x, (int) z), z);

                this.c = to.getX();
                this.d = to.getY();
                this.e = to.getZ();

                return true;//will run the c();
            } else return false;
        }
    }

    public void c() {
        //runner / here i can put the navigation (x - y - z - speed)
        this.a.getNavigation().a(this.c, this.d, this.e, this.f);
    }

    public boolean b() {
        //runs after c();
        return !this.a.getNavigation().m() && this.b.h(this.a) < (double) (this.g * this.g);
    }

    public void d() {
        //will runs if the b is false
        this.b = null;
    }
}
